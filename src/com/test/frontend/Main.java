package com.test.frontend;

import com.test.enums.GradeType;
import com.test.enums.SchoolType;
import com.test.exceptions.AgeNotCorrectException;
import com.test.exceptions.ClassFullException;
import com.test.exceptions.GradeNotCorrectException;
import com.test.service.*;

import java.time.LocalDate;
import java.time.Period;
import java.util.Scanner;
import java.util.UUID;

import static java.lang.System.*;

public class Main {
    private static PreSchool preSchool= new PreSchool();
    private  static ElementarySchool elementarySchool = new ElementarySchool();
         private  static MiddleSchool middleSchool = new MiddleSchool();
    private static HighSchool highSchool = new HighSchool();
    private static  Scanner scanner = new Scanner(in);
        public static void main(String[] args) throws ClassFullException {
        //1. Show the prompts on console  - done
            do {
                out.println("Welcome to School Admissions App, Press X for Exit");
                out.println("Enter the name of the student");
                String name = scanner.next();
                studentNameValidation(name);
                Integer studentAge = studentAgeValidation();
                GradeType grade = determineGradeBasedOnAge(studentAge);
                SchoolType school = determineSchoolBasedOnGrade(grade);
                Student student = new Student(name, studentAge, grade);
                School school1 = retrieveSchoolsByGrades(school);
                Student student1 = school1.admitStudent(student);
                Double fees = school1.chargeFees(student);
                out.println(fees);
                String uuid = determineStudentID();
                student.setUUID(uuid);
                out.println(student1.toString());
                out.println("enter X if you want to exit");
            }while (!(scanner.next().equalsIgnoreCase("X")));

    }

    private static String determineStudentID() {
            UUID randomUUID = UUID.randomUUID();
        return randomUUID.toString();
    }

    private static School retrieveSchoolsByGrades(SchoolType schoolType) {
        switch (schoolType){
            case HIGH_SCHOOL:
                return highSchool;
            case ELEMENTARY_SCHOOL:
                return elementarySchool;
            case MIDDLE_SCHOOL:
                return middleSchool;
            case PRE_SCHOOL:
                return preSchool;
            default:
                throw new IllegalArgumentException("wrong school type");
        }

    }


    private static SchoolType determineSchoolBasedOnGrade(GradeType grade) {
        switch (grade) {
            case JK_GRADE:
            case SK_GRADE:
                return SchoolType.PRE_SCHOOL;
            case FIRST_GRADE:
            case FIFTH_GRADE:
            case SECOND_GRADE:
            case THIRD_GRADE:
            case FOURTH_GRADE:
                return SchoolType.ELEMENTARY_SCHOOL;
            case SIXTH_GRADE:
            case SEVENTH_GRADE:
            case EIGHTH_GRADE:
                return SchoolType.MIDDLE_SCHOOL;
            case NINTH_GRADE:
            case TENTH_GRADE:
            case ELEVENTH_GRADE:
            case TWELTH_GRADE:
                return SchoolType.HIGH_SCHOOL;
            default:
                try {
                    throw new GradeNotCorrectException();
                } catch (GradeNotCorrectException e) {
                    out.println("no");
                }

        }
        return null;
    }

    private static GradeType determineGradeBasedOnAge(Integer age) {
        switch (age) {
            case 4:
                return GradeType.JK_GRADE;
            case 5:
                return GradeType.SK_GRADE;
            case 6:
                return GradeType.FIRST_GRADE;
            case 7:
                return GradeType.SECOND_GRADE;
            case 8:
                return GradeType.THIRD_GRADE;
            case 9:
                return GradeType.FOURTH_GRADE;
            case 10:
                return GradeType.FIFTH_GRADE;
            case 11:
                return GradeType.SIXTH_GRADE;
            case 12:
                return GradeType.SEVENTH_GRADE;
            case 13:
                return GradeType.EIGHTH_GRADE;
            case 14:
                return GradeType.NINTH_GRADE;
            case 15:
                return GradeType.TENTH_GRADE;
            case 16:
                return GradeType.ELEVENTH_GRADE;
            case 17:
                return GradeType.TWELTH_GRADE;
            default:
                try {
                    throw new AgeNotCorrectException("age has to be between 4 and 17");
                } catch (AgeNotCorrectException e) {
                    out.println("age has to enter above 4");
                }
        }

        return null;
    }

    private static String studentNameValidation(String name) {
        if (name.isEmpty() && (name.length() >= 3 && name.length() <= 50)) {
            return "name of the student is too long";
        } else
            return name;
    }

    private static Integer studentAgeValidation() {
        out.print("Please enter date of birth in YYYY-MM-DD: ");
        Scanner scannerForDOB = new Scanner(in);
        String birthday = scannerForDOB.nextLine();
        LocalDate currentDate= LocalDate.now();
        LocalDate dob= LocalDate.parse(birthday);
        Period period= Period.between(dob, currentDate);
        Integer agePeriod= period.getYears();
        if (agePeriod > 4 || agePeriod <= 17) return agePeriod;
        else {
            return -1;
        }

    }
}