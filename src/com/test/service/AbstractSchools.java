package com.test.service;

import com.test.exceptions.ClassFullException;

public interface AbstractSchools {
    Student admitStudent(Student student) throws ClassFullException;
}
