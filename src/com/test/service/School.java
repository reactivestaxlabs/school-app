package com.test.service;

import com.test.exceptions.ClassFullException;

public interface School {
    Student admitStudent(Student student) throws ClassFullException;
    Double chargeFees(Student student);
}
