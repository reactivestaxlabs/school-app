package com.test.service;

import com.test.enums.GradeType;
import com.test.exceptions.ClassFullException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public abstract class AbstractSchool implements School {
    Map<GradeType, List<Student>> gradeToStudentsMap = new HashMap<>();

    private Map<GradeType, List<Student>> gradeToListOfStudentsMap = new HashMap<GradeType, List<Student>>();

    @Override
    public Student admitStudent(Student student) throws ClassFullException {

        //admit the student in the appropriate grade
        List<Student> listOfStudents = gradeToListOfStudentsMap.get(student.getGrade());
        if (listOfStudents == null) {
            listOfStudents = new ArrayList<>();
            gradeToListOfStudentsMap.put(student.getGrade(), listOfStudents);
            listOfStudents.add(student);
        } else {
            if (listOfStudents.size() > 3) {
                throw new ClassFullException();
            }
            listOfStudents.add(student);
        }
        //return the student
        return student;

    }

}
