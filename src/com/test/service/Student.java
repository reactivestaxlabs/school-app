package com.test.service;

import com.test.enums.GradeType;

public class Student {
       private  String UUID;
       private String name;
         private int age;
     private GradeType grade;

        public Student(String name, int age, GradeType grade) {

            this.name = name;
            this.age = age;
            this.grade = grade;
        }

        public void setUUID(String UUID) {
            this.UUID = UUID;
        }

        @Override
        public String toString() {
            return "Student Details  "+" \n " + "------------- " + "\n " +
                    "UUID ::    " + UUID + "\n" +
                    " name ::   " + name + "\n" +
                    " age ::    " + age + "\n" +
                    " grade ::  " + grade ;
        }

        public String getUUID() {
            return UUID;
        }

        public String getName() {
            return name;
        }

        public int getAge() {
            return age;
        }

        public GradeType getGrade() {
            return grade;
        }

        public void setName(String name) {
            this.name = name;
        }

        public void setAge(int age) {
            this.age = age;
        }

        public void setGrade(GradeType grade) {
            this.grade = grade;
        }
    }

